# Maven 4 Tools

This is a small utility for taking the Maven 3.x codebase and transforming it what the Maven 4.x codebase will look like taking into account the following:

- Removal deprecated methods
- Removal deprecated files
- Removal deprecated modules
- Updating to remove deprecated dependencies

The tool removes deprecated code by using [Munge][1] preprocessor looking for the following markers in the Maven code base:

```java
/*if_not[MAVEN4]*/

//
// Deprecated 
//
        
private PlexusContainer container;        
private final Settings settings;

@Deprecated
public Object lookup( String role ) throws ComponentLookupException
{
    return container.lookup( role );
}

...
         
/*end[MAVEN4]*/
    
```

When we run Munge this code will be removed and all similar blocks like it.

To transform your Maven 3.x codebase to a Maven 4.x codebase just open up the project in your IDE, edit the `main()` to match your desired input/output directories and run. In the output directory you will have a functioning Maven build with all the transformation actions performed.

[1]: https://publicobject.com/2009/02/preprocessing-java-with-munge.html