package io.takari.maven4;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

import org.codehaus.plexus.util.FileUtils;

import com.google.common.base.Charsets;

public class Maven4 {

  private final File mavenSourceTree;
  private final File transformedMavenSourceTree;

  public Maven4(File mavenSourceTree, File transformedMavenSourceTree) {
    this.mavenSourceTree = mavenSourceTree;
    this.transformedMavenSourceTree = transformedMavenSourceTree;
  }

  public void createTree() throws Exception {


    // remove deprecated methods
    // remove deprecated files
    // remove deprecated modules
    // update dependencies

    FileUtils.deleteDirectory(transformedMavenSourceTree);

    Munge.symbols.put("MAVEN4", Boolean.TRUE);

    //
    // Run the munge pre-processor over all the Java sources removing all the deprecated code
    //    
    munge( //
        mavenSourceTree.getAbsolutePath(), // source
        transformedMavenSourceTree.getAbsolutePath(), // target
        "**/*.java", // includes
        "maven-compat/**" // excludes
    );

    //
    // Copy over the rest of the project structure excluding certain files along with
    // excluding all the Java files we processed already.
    //
    final Path sourcePath = mavenSourceTree.toPath();
    final Path targetPath = transformedMavenSourceTree.toPath();
    Files.walkFileTree(sourcePath, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
        String path = sourcePath.relativize(file).toString();
        if ( //
            path.startsWith(".git") || //
            path.startsWith("maven-compat") || //
            path.contains(".settings") || //
            path.contains("target") || //
            path.contains(".project") || //
            path.contains(".classpath") || //
            path.endsWith(".java")) {
          return FileVisitResult.CONTINUE;
        }
        Files.createDirectories(targetPath.resolve(sourcePath.relativize(file.getParent())));
        Files.copy(file, targetPath.resolve(sourcePath.relativize(file)));
        return FileVisitResult.CONTINUE;
      }
    });
    
    //
    // Modify the MMP POM to remove the maven-comppat module. Replace with Java7 i/o
    //
    String pom = com.google.common.io.Files.toString(new File(mavenSourceTree, "pom.xml"), Charsets.UTF_8);
    pom = pom.replace("<module>maven-compat</module>", "");
    com.google.common.io.Files.write(pom, new File(transformedMavenSourceTree, "pom.xml"), Charsets.UTF_8);
  }

  /**
   * Munges source files found in {@code from} and places them in {@code to}, honoring any includes or excludes.
   * 
   * @param from The original source directory
   * @param to The munged source directory
   * @param includes Comma-separated list of files to include
   * @param excludes Comma-separated list of files to exclude
   * @throws MojoExecutionException
   */
  @SuppressWarnings("unchecked")
  public static void munge(final String from, final String to, final String includes, final String excludes) throws Exception {
    try {
      for (final File f : (List<File>) FileUtils.getFiles(new File(from), includes, excludes)) {
        final String inPath = f.getPath();
        final String outPath = inPath.replace(from, to);
        new File(outPath).getParentFile().mkdirs();
        final Munge munge = new Munge(inPath, outPath);
        munge.process();
        munge.close();
      }
    } catch (final IOException e) {
      throw new Exception(e.toString());
    }
  }  
  
  public static void main(String[] args) throws Exception {
    File mavenSourceTree = new File("/Users/jvanzyl/js/tesla/maven/");
    File transformedMavenSourceTree = new File("/tmp/maven");
    Maven4 m4 = new Maven4(mavenSourceTree, transformedMavenSourceTree);
    m4.createTree();
  }

}
